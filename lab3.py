#!/usr/bin/env python
import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import ComplementNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier

from common import describe_data, test_env
from common.classification_metrics import print_metrics
from common.describe_data import print_overview, print_categorical


def read_data(file):
    """Return pandas dataFrame read from csv file"""
    try:
        return pd.read_excel(file)
    except FileNotFoundError:
        exit('ERROR: ' + file + ' not found')


def preprocess_data(df, verbose=False):
    y_column = 'In university after 4 semesters'

    # Features can be excluded by adding column name to list
    drop_columns = []

    categorical_columns = [
        'Faculty',
        'Paid tuition',
        'Study load',
        'Previous school level',
        'Previous school study language',
        'Recognition',
        'Study language',
        'Foreign student'
    ]

    # Handle dependent variable
    if verbose:
        print('Missing y values: ', df[y_column].isna().sum())

    y = df[y_column].values
    # Encode y. Naive solution
    y = np.where(y == 'No', 0, y)
    y = np.where(y == 'Yes', 1, y)
    y = y.astype(float)

    # Drop also dependent variable variable column to leave only features
    drop_columns.append(y_column)
    df = df.drop(labels=drop_columns, axis=1)

    # Remove drop columns for categorical columns just in case
    categorical_columns = [
        i for i in categorical_columns if i not in drop_columns]

    # STUDENT SHALL ENCODE CATEGORICAL FEATURES
    for column in categorical_columns:
        df[column] = df[column].fillna(value='Missing')
        df = pd.get_dummies(df, prefix=[column], columns=[column])

    # df = df.drop(df.columns[0], axis=1)

    # Handle missing data. At this point only exam points should be missing
    # It seems to be easier to fill whole data frame as only particular columns
    if verbose:
        describe_data.print_nan_counts(df)

    # STUDENT SHALL HANDLE MISSING VALUES
    df = df.fillna(value=0)

    if verbose:
        describe_data.print_nan_counts(df)

    # Return features data frame and dependent variable
    return df, y


# STUDENT SHALL CREATE FUNCTIONS FOR LOGISTIC REGRESSION CLASSIFIER, KNN
# CLASSIFIER, SVM CLASSIFIER, NAIVE BAYES CLASSIFIER, DECISION TREE
# CLASSIFIER AND RANDOM FOREST CLASSIFIER
def train_test_clf(X_test, X_train, y_test, y_train, classifier, verbose):
    if verbose:
        print('# Using:', classifier['label'])
        print('Classifier:', classifier['clf'])
        print('Scaler:', classifier['sc'], '\n')

    if classifier['sc'] is not None:
        X_train = classifier['sc'].fit_transform(X_train)
        X_test = classifier['sc'].transform(X_test)

    classifier['clf'].fit(X_train, y_train)
    print_metrics(y_test,
                  classifier['clf'].predict(X_test),
                  classifier['label'] + ' test data',
                  verbose=verbose)
    return


def train_test_classifiers(X_df, y, classifiers, verbose=False):
    X = X_df.values
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.25, random_state=0)

    for classifier in classifiers:
        train_test_clf(X_test, X_train, y_test, y_train, classifier, verbose)

    return


if __name__ == '__main__':
    modules = ['numpy', 'pandas', 'sklearn']
    test_env.versions(modules)

    students = read_data('data/students.xlsx')

    # STUDENT SHALL CALL PRINT_OVERVIEW AND PRINT_CATEGORICAL FUNCTIONS WITH
    # FILE NAME AS ARGUMENT
    print_overview(students, file='results/students_overview.txt')
    print_categorical(students, file='results/students_categorical_data.txt')

    students_filtered = students[students["In university after 4 semesters"] == "Yes"]
    print_overview(students_filtered,
                   file='results/students_overview_filtered.txt')
    print_categorical(students_filtered,
                      file='results/students_categorical_data_filtered.txt')

    students_X, students_y = preprocess_data(students)

    classifiers = [
        {
            'label': 'Logistic regression',
            'clf': LogisticRegression(random_state=0, solver='sag'),
            'sc': StandardScaler()
        },
        {
            'label': 'KNN',
            'clf': KNeighborsClassifier(n_neighbors=6),
            'sc': StandardScaler()
        },
        {
            'label': 'SVC',
            'clf': SVC(kernel='sigmoid', random_state=0, gamma=0.03, probability=True, tol=4e-3),
            'sc': StandardScaler()
        },
        {
            'label': 'Naive Bayes',
            'clf': ComplementNB(),
            'sc': None
            # Using a standard scaler here will throw an error. Not sure why that is.
        },
        {
            'label': 'Decision Tree',
            'clf': DecisionTreeClassifier(random_state=0),
            'sc': StandardScaler()
        },
        {
            'label': 'Random Forest',
            'clf': RandomForestClassifier(n_estimators=100, random_state=0),
            'sc': StandardScaler()
        }
    ]

    # STUDENT SHALL CALL CREATED CLASSIFIERS FUNCTIONS.
    train_test_classifiers(students_X, students_y, classifiers, True)

    print('Done')
